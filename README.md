# SIXLIV3 Thin ZSH Theme

This is a forked and modified version of the [theunraveler](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes#theunraveler) [oh my zsh](http://ohmyz.sh/) theme.

When inside a git repository the directory path in the prompt will be relative to the git root path.

## Preview

![Theme Screenshot](docs/images/sixlive-theme.png)

## Installation

1. Ensure your `ZSH_CUSTOM` variable is set in your `.zshrc` file (`ZSH_CUSTOM=$HOME/.oh-my-zsh-custom`)
2. In your `ZSH_CUSTOM` add the `sixlive.zsh-theme` to `themes/`
3. Activate the theme `ZSH_THEME="sixlive"` in your `.zshrc` file

Custom theme directory example after install:

```
/Users/tjmiller/.oh-my-zsh-custom/
└── themes
    └── sixlive.zsh-theme
```

## Colors

I've included my iTerm colors scheme.

1. Launch iTerm 2
2. Type CMD+i (⌘+i)
3. Navigate to Colors tab
4. Click on Load Presets
5. Click on Import
6. Select the `sixlive.itermcolors` file
7. Click on Load Presets and choose the color scheme

## Font

I'm using [sauce code pro](https://github.com/powerline/fonts) w/ powerline support.
